class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def get_cookies
    session[:auth_token] = cookies.signed[:auth_token] if !cookies.signed[:auth_token].nil?
    session[:id] = cookies.signed[:id] if !cookies.signed[:id].nil?
  end

  def check_logged
    get_cookies
    redirect_to :login if session[:auth_token].nil?
  end

  def check_not_logged
    get_cookies
    redirect_to :root if !session[:auth_token].nil?
  end

end
