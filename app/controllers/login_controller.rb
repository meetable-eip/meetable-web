class LoginController < ApplicationController

  before_action :check_not_logged
  
  def initialize
    super
    @api = Faraday.new(:url => 'http://api.meetable.fr') do |faraday|
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter
    end
  end

  def new
  end

  def create
    response = @api.post('/signin', login_params)
    result = JSON.parse(response.body).with_indifferent_access
    if response.status == 200
      session[:auth_token] = result[:auth_token]
      session[:id] = result[:id]
      cookies.permanent.signed[:auth_token] = result[:auth_token]
      cookies.permanent.signed[:id] = result[:id]
      redirect_to :root
    else
      flash.now[:alert] = result[:error]
      render :new
    end
  end

  def login_params
    params.permit(:login, :password)
  end

end
