class HomeController < ApplicationController

  before_action :check_logged

  def initialize
    super
    @api = Faraday.new(:url => 'http://api.meetable.fr') do |faraday|
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter
    end
  end

  def index
    response = @api.get("/users/#{session[:id]}", { auth_token: session[:auth_token] })
    if response.status == 200
      @result = JSON.parse(response.body).with_indifferent_access
    else
      flash[:alert] = 'Api request failed'
    end
  end

  def edit
  end

  def update
    args = {}
    p params
    args[:auth_token] = session[:auth_token]
    if !params[:login].blank?
      args[:login] = params[:login]
    end
    if !params[:email].blank?
      args[:email] = params[:email]
    end
    if !params[:password].blank?
      args[:password] = params[:password]
    end
    response = @api.put("/users/#{session[:id]}", args)
    result = JSON.parse(response.body).with_indifferent_access
    if response.status == 200
      flash[:success] = "Data successfully saved"
      redirect_to :settings
    else
      flash[:alert] = result[:error]
      redirect_to :settings
    end
  end

end
