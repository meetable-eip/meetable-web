class LogoutController < ApplicationController

  def destroy
    session.delete(:auth_token)
    session.delete(:id)
    cookies.delete :auth_token
    cookies.delete :id
    redirect_to :root
  end

end
