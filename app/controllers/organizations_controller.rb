class OrganizationsController < ApplicationController

  def initialize
    super
    @api = Faraday.new(:url => 'http://api.meetable.fr') do |faraday|
      faraday.request  :url_encoded
      faraday.response :logger
      faraday.adapter  Faraday.default_adapter
    end
  end

  def index
    response = @api.get("/organizations", { auth_token: session[:auth_token] })
    if response.status == 200
      @organizations = JSON.parse(response.body)
    else
      flash[:alert] = 'Api request failed'
      redirect_to :logout
    end
  end

  def show
  end

  def new
  end

  def create
    args = {}
    p params
    args[:auth_token] = session[:auth_token]
    if !params[:name].blank?
      args[:name] = params[:name]
    end
    if !params[:description].blank?
      args[:description] = params[:description]
    end
    response = @api.post("/organizations", args)
    result = JSON.parse(response.body).with_indifferent_access
    if response.status == 200
      flash[:success] = "Data successfully saved"
      redirect_to :organizations
    else
      flash[:alert] = result[:error]
      redirect_to :organizations_new
    end
  end

end
